package lab06;

public class subclassoverload
{
    public void disp(char c)
    {
         System.out.println(c);
    }
    public void disp(char c, int num)  
    {
         System.out.println(c + " "+num);
    }
}
class Sample extends subclassoverload 
{
   public static void main(String args[])
   {
	   subclassoverload obj = new subclassoverload();
       obj.disp('a');
       obj.disp('a',10);
   }
}