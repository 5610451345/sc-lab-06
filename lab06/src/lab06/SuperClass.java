package lab06;
public abstract class SuperClass{
  public abstract SuperClass getSelf();
  abstract public void anotherMethod();
  public abstract double apply( double operand );
  public abstract double apply( double first, double second );
}

class SubClass extends SuperClass{
  @Override
  public SubClass getSelf(){
    return this;
  }

@Override
public void anotherMethod() {
	// TODO Auto-generated method stub
	}

@Override
public double apply(double operand) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public double apply(double first, double second) {
	// TODO Auto-generated method stub
	return 0;
}
}

class SubClass2 extends SuperClass{


	@Override
	public void anotherMethod() {
		// TODO Auto-generated method stub
		System.out.print("@Override");
		}

	@Override
	public SuperClass getSelf() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double apply(double operand) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double apply(double first, double second) {
		// TODO Auto-generated method stub
		return 0;
	}
}

class SubClass3 extends SuperClass{


	@Override
	public void anotherMethod() {
		// TODO Auto-generated method stub
		}

	@Override
	public SuperClass getSelf() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double apply(double operand) {
		// TODO Auto-generated method stub
		return ++operand;
	}

	@Override
	public double apply(double first, double second) {
		// TODO Auto-generated method stub
		return first * second;
	}
	
	   public static void main(String args[])
	    {
		   SubClass3 obj = new SubClass3();
	        obj.apply(0);
	        obj.apply(5,6);
	    }

	
}